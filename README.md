# maintenance-page

This repo is a single maintenance page for demo purposses.

## Deployment steps

```
gcloud app deploy --project ${GCP_PROJECT} --quiet
```

## Thanks to:

- https://codingislove.com/site-maintenance-page-template-free-download/